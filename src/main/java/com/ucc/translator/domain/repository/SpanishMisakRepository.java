package com.ucc.translator.domain.repository;

import com.ucc.translator.domain.entity.Misak_spanish;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SpanishMisakRepository extends JpaRepository<Misak_spanish, Long> {

        List<Misak_spanish> findAllByIdMisak(Long idMisak);
    }



