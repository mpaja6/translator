package com.ucc.translator.domain.repository;

import com.ucc.translator.domain.entity.Words_spanish;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface SpanishRepository extends JpaRepository<Words_spanish, Long> {

    List<Words_spanish> findAllByOrderByWords();

    //select * from product p order by p.name
    @Query(value = "select ws from Words_spanish ws order by  ws.words")
    List<Words_spanish> getAllOrderByWords();

    List<Words_spanish> findByWordsContains(String words);

    @Query(value = "select ws from Words_spanish ws where  ws.words like  :words")
    List<Words_spanish> getAllByWords_SpanishIsLike(@Param("words") String words);


    @Query(value = "select ws from Words_spanish ws where  ws.words   =:words")
    Optional<Words_spanish> findByWords(@Param("words") String words);

    /**
     * select p.* from product p
     * inner join product_category pc on (p.id = pc.id_product)
     * inner join category c on (c.id = pc.id_category)
     * where c.name = 'lacteos naturales';
     **/
    // utiliza hints directamente en base de datos
    @Query(value = "select p from Words_spanish p " +
            "inner join Misak_spanish pc on (p.id = pc.idSpanish)" +
            "inner join Words_misak c on (c.id = pc.idMisak) " +
            "where c.words = :misakWords order by p.description desc ")
    List<Words_spanish> getAllByMisakWords(@Param("misakWords") String misakWords);

    @Query(value = "select sum(p.id) from Words_spanish p " +
            "inner join Misak_spanish pc on (p.id = pc.idSpanish)" +
            "inner join Words_misak c on (c.id = pc.idMisak) " +
            "where c.words = :misakWords order by p.description desc ")
    Optional<Integer> sumSpanishByMisakWords(@Param("misakWords") String misakWords);

    // utd hizo el query, utd se encarga del rendimiento
    @Query(value = "select ws.* from words_spanish ws" +
            "inner join spanish_misak sm on (ws.id=sm.id_spanish)" +
            "inner join words_misak wm on (wm.id=sm.id_misak)" +
            "where wm.words = :misakWords", nativeQuery = true)
    List<Words_spanish> getAllByMisakWordsNative(@Param("misakWords") String misakWords);


}


