package com.ucc.translator.domain.repository;

import com.ucc.translator.domain.entity.Words_misak;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

    public interface MisakRepository extends JpaRepository<Words_misak, Long> {
        /**
         *CAPA DE ACCESO A DATOS -  REPOSITORIO DE CONSULTAS - DAO (Data Acces Object)
         */
        //CRUD
        @Override
        List<Words_misak> findAll();
        Optional<Words_misak> findById(Long id);
        Words_misak save(Words_misak words_misak);
        void  deleteById(Long id);

    }



