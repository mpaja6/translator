package com.ucc.translator.domain.dto;

public class SpanishForm {

    private long id;
    private String words;
    private String description;
    private long idMisak;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getWords() {
        return words;
    }

    public void setWords(String words) {
        this.words = words;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getIdMisak() {
        return idMisak;
    }

    public void setIdMisak(long idMisak) {
        this.idMisak = idMisak;
    }
}