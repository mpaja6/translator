package com.ucc.translator.domain.dto;

public class SpanishDTO {
    private Long id;
    private String words;
    private String description;
    private MisakDTO misak;

    public MisakDTO getMisak() {
        return misak;
    }

    public void setMisak(MisakDTO misak) {
        this.misak = misak;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWords() {
        return words;
    }

    public void setWords(String words) {
        this.words = words;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
