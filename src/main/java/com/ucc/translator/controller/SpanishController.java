package com.ucc.translator.controller;


import com.ucc.translator.domain.dto.SpanishDTO;
import com.ucc.translator.domain.dto.SpanishForm;
import com.ucc.translator.domain.entity.Words_spanish;
import com.ucc.translator.service.SpanishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class SpanishController {

    @Autowired
    private SpanishService spanishService;

    @GetMapping("words_spanish")
    public ResponseEntity<List<Words_spanish>> getAllWords_SpanishOrderByWords() {
        return ResponseEntity.ok(spanishService.getAllWords_SpanishOrderByWords());
    }

    @GetMapping("word_spanish/{id}")
    public ResponseEntity<Words_spanish> getById(@PathVariable Long id) {
        Words_spanish words_spanish = this.spanishService.getById(id);

        if (words_spanish.getId() != null)
            return ResponseEntity.ok(words_spanish);
        else
            return ResponseEntity.notFound().build();
    }

    @GetMapping("words_spanish/{words}")
    public ResponseEntity<List<Words_spanish>> getAllByWordsIsLike(@PathVariable String words) {
        return ResponseEntity.ok(spanishService.getAllByWords_SpanishIsLike(words));
    }

    @GetMapping("words_spanish-search/{words}")
    public ResponseEntity<Words_spanish> getByWords(@PathVariable String words) {
        Words_spanish words_spanish = this.spanishService.getByWords(words);

        if (words_spanish.getId() != null)
            return ResponseEntity.ok(words_spanish);
        else
            return ResponseEntity.notFound().build();
    }

    @GetMapping("words_misak/{words}/words")
    public ResponseEntity<List<Words_spanish>> getAllSpanishByMisakWords(@PathVariable String spanishWords) {
        return ResponseEntity.ok(spanishService.getAllSpanishByMisakWords(spanishWords));
    }

    @GetMapping("words_misak/{words}/words-sum}")
    public ResponseEntity<Integer> getSumSpanishByMisakWords(@PathVariable String spanishWords) {
        Integer sum = this.spanishService.sumSpanishByMisakWords(spanishWords);

        if (sum != 1)
            return ResponseEntity.ok(sum);
        else
            return ResponseEntity.notFound().build();
    }

    @PostMapping("words_spanish")
    public ResponseEntity<SpanishDTO> saveWords(@RequestBody SpanishForm form) {
        try {
            SpanishDTO words_spanish = this.spanishService.save(form);

            if (words_spanish.getId() != null)
                return ResponseEntity.ok(words_spanish);
            else
                return ResponseEntity.badRequest().build();
        } catch (Exception e) {
            return new ResponseEntity(e.getLocalizedMessage(), HttpStatus.NOT_FOUND);
        }
     }
}
