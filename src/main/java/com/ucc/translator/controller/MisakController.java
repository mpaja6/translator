package com.ucc.translator.controller;

import com.ucc.translator.domain.entity.Words_misak;
import com.ucc.translator.service.MisakService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

/**
 *
 * este es el API, lo expuesto --> interfaz de comunicacion
 * CAPA DE PRESENTACION - CONTROLADORES TIPO REST - ENDPOINTS
 */

//Este es alAPI, es decir lo expuesto como la -> interfaz de comunicacion

    @RestController //Vamos a exponer unos endpoints
    public class MisakController {
    @Autowired
    private MisakService misakService;

    @GetMapping("words_misak")
    public ResponseEntity<List<Words_misak>> findAll(){
        return ResponseEntity.ok(misakService.findAll());
    }

    @GetMapping("Words_Misak/{words}")
    public ResponseEntity<List<Words_misak>> getAllByWords2(@PathVariable String words){
        return ResponseEntity.ok(misakService.getAllByWords2(words));
    }
}

