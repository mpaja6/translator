package com.ucc.translator.service;

import com.ucc.translator.domain.entity.Words_misak;

import java.util.List;
import java.util.Optional;

public interface MisakService {
    /**
     *CAPA DE LOGICA DE NEGOCIO - PROGRAMACION - SERVICIOS (microservicios)
     * PATRON FACADE
     */
    List<Words_misak> findAll();

    List<Words_misak> getAllByWords2(String words);

    Optional<Words_misak> findById(Long id);

    Words_misak save(Words_misak words_misak);

    void  deleteById(Long id);




}
