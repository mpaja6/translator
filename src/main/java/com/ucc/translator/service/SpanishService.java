package com.ucc.translator.service;

import com.ucc.translator.domain.dto.SpanishDTO;
import com.ucc.translator.domain.dto.SpanishForm;
import com.ucc.translator.domain.entity.Words_misak;
import com.ucc.translator.domain.entity.Words_spanish;

import java.util.List;

public interface SpanishService {

    List<Words_spanish> getAllWords_SpanishOrderByWords();

    List<Words_spanish> getAllByWords_SpanishIsLike(String words);

    Words_spanish getById(long id);
    Words_spanish getByWords(String words);

    List<Words_spanish> getAllSpanishByMisakWords(String misakWords);

    Integer sumSpanishByMisakWords(String misakWords);

    SpanishDTO save(SpanishForm form);


}

