package com.ucc.translator.service;

import com.ucc.translator.domain.entity.Words_misak;
import com.ucc.translator.domain.repository.MisakRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service //Lo expone como un seervicio
@Transactional //punto a punto (commit) sino hace un Rollback

public class MisakServiceImpl implements MisakService {

    @Autowired //inyeccion de dependencias, capa de acceso de datos y logina de negocio
    private MisakRepository misakRepository;

    @Override
    public List<Words_misak> findAll() {
        return misakRepository.findAll();
    }

    @Override
    public List<Words_misak> getAllByWords2(String words) {
        return null;
    }

    @Override
    public Optional<Words_misak> findById(Long id) {
        return Optional.empty();
    }

    @Override
    public Words_misak save(Words_misak words_misak) {
        return null;
    }

    @Override
    public void deleteById(Long id) {

    }


}
