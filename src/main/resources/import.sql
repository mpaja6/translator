INSERT INTO words_spanish (words,description) VALUES ('Abeja','Insecto de unos 15 mm de largo, de color pardo oscuro y con vello rojizo, con dos pares de alas transparentes cruzadas de nervios');
INSERT INTO words_spanish (words,description) VALUES ('Abuelo','Es el padre  de sus padres, siendo respectivamente llamados abuelo');
INSERT INTO words_spanish (words,description) VALUES ('Abuela','Es la madre  de sus padres, siendo respectivamente llamados abuela');


INSERT INTO words_misak (words) VALUES ('mawei');
INSERT INTO words_misak (words) VALUES ('shur');
INSERT INTO words_misak (words) VALUES ('shura');

INSERT INTO spanish_misak (id_spanish,id_misak) VALUES (1,1);
INSERT INTO spanish_misak (id_spanish,id_misak) VALUES (2,2);
INSERT INTO spanish_misak (id_spanish,id_misak) VALUES (3,3);